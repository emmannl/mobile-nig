<?php


namespace Emmannl\MobileNg;


use Emmannl\MobileNg\Exception\RemoteServiceException;
use GuzzleHttp\Client;

trait HttpRequest
{
    /**
     * Make a GET request to the MobileNig endpoint
     * @param string $url
     * @param array $query_params
     * @param array $options
     * @return string
     * @throws RemoteServiceException
     */
    protected function get(string $url, array $query_params = [], array $options = []): string
    {
        $client = new Client();
        $response = $client->request('GET', $url, [
            'query' => $query_params,
        ]);

        $result =  (string) $response->getBody();

        if (! $this->throw_exceptions) {
            return $result;
        }

        $result_array = json_decode($result, true);

        if (isset($result_array['code']) && isset($result_array['description'])) {
            throw new RemoteServiceException($result_array);
        }

        return $result;
    }
}