<?php


namespace Emmannl\MobileNg\Services;



use Emmannl\MobileNg\Exception\IncompleteOptionsException;
use Emmannl\MobileNg\HttpRequest;
use Emmannl\MobileNg\MobileNigService;


class Data extends MobileNigService
{
    use HttpRequest;

    /**
     * @var array
     */
    protected $products = [];

    protected const TEST_API_URL = "https://mobilenig.com/API/data_test";
    protected const LIVE_API_URL = "https://mobilenig.com/API/data";
    protected const TXN_INFO_API_URL = "https://mobilenig.com/API/data_query";
    protected const QUERY_API_URL = "https://mobilenig.com/API/data_query";


    public function __construct(string $username, string $api_key, bool $test_mode = false, bool $throw_exceptions = true)
    {
        parent::__construct($username, $api_key, $test_mode, $throw_exceptions);

        $this->setProductsAndPrices();
    }

    protected function setProductsAndPrices(): void
    {
        $this->products = $this->getProductsAndPrices();
    }


    public function getProductsAndPrices(): ?array
    {
        $data = file_get_contents(__DIR__ . "/../data/data_product.json");
        return json_decode($data, true);
    }

    public function productInfo(string $product_code): ?array
    {
        $products = $this->products ?? $this->getProductsAndPrices();

        foreach ($products as $key =>  $product) {
            foreach ($product as $item) {
                if ($item['product_code'] === $product_code) {
                    return array_merge($item, ['network' => $key]);
                }
            }
        }

        return null;
    }

    public function products(): ?array
    {
        $products = $this->products ?? $this->getProductsAndPrices();

        if (is_null($products)) return null;

        $flattened_products = [];

        foreach ($products as $key => $datum) {
            foreach ($datum as $item) {
                $flattened_products[] = array_merge($item, ['network' => $key]);
            }
        }

        return $flattened_products;
    }

    public function buy(array $options): ?array
    {
        $this->validateRequiredOptions($options);

        $options = array_merge($options, $this->credentials);


        $result =  $this->get(
            $this->test_mode ? self::TEST_API_URL : self::LIVE_API_URL,
            $options
        );

        return  json_decode($result, true);
    }


    public function transactionInfo(string $trans_id): ?array
    {
        $params = array_merge($this->credentials, compact('trans_id'));
        $result = $this->get(self::TXN_INFO_API_URL, $params);

        return json_decode($result, true);
    }

    protected function validateRequiredOptions($options): void
    {
        $required_options = [
            'network',
            'phoneNumber',
            'price',
            'product_code',
            'trans_id',
            'return_url',
        ];


        foreach ($required_options as$required_option) {
            if (empty($options[$required_option])) {
                throw new IncompleteOptionsException($required_option);
            }
        }
    }

    public function queryOrder($trans_id): ?array
    {
        $result = $this->get(
            self::QUERY_API_URL,
            array_merge($this->credentials, compact('trans_id'))
            );

        return json_decode($result, true);
    }
}