<?php


namespace Emmannl\MobileNg\Services;


use Emmannl\MobileNg\Exception\IncompleteOptionsException;
use Emmannl\MobileNg\HttpRequest;
use Emmannl\MobileNg\MobileNigService;

class Airtime extends MobileNigService
{
    use HttpRequest;

    private const PREMIUM_LIVE_URL = "https://mobilenig.com/API/airtime_premium";
    private const PREMIUM_TEST_URL = "https://mobilenig.com/API/airtime_premium_test";
    protected const QUERY_API_URL = "https://mobilenig.com/API/airtime_premium_query";

    /**
     * Buy airtime
     * @param array $options
     * @return array|null
     * @throws \Emmannl\MobileNg\Exception\RemoteServiceException
     */
    public function buy(array $options): ?array
    {
        $this->validateOptions($options);

        $url = $this->test_mode ? self::PREMIUM_TEST_URL : self::PREMIUM_LIVE_URL;
        $result = $this->get($url, $options);
        return json_decode($result, true);
    }

    public function getProductsAndPrices() {}

    protected function validateOptions(array $submission)
    {
        $required = [
            'network',
            'phoneNumber',
            'amount',
            'trans_id',
        ];

        foreach ($required as $item) {
            if (empty($submission[$item])) {
                throw new IncompleteOptionsException($item);
            }
        }
    }

    public function queryOrder($trans_id)
    {
        $result = $this->get(
            self::QUERY_API_URL,
            array_merge($this->credentials, compact('trans_id'))
        );

        return json_decode($result, true);
    }
}