<?php


namespace Emmannl\MobileNg;



abstract  class MobileNigService
{

    use Initialiser;

    public abstract function buy(array $options);

    public abstract function getProductsAndPrices();

    public abstract function queryOrder($trans_id);

}