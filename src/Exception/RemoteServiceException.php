<?php


namespace Emmannl\MobileNg\Exception;


use Throwable;

class RemoteServiceException extends \Exception
{
    public function __construct(array $result)
    {
        $message  = "The Remote service Returned an error: Code ({$result['code']}): {$result['description']}";
        parent::__construct($message);
    }
}