<?php


namespace Emmannl\MobileNg\Exception;


use Throwable;

class IncompleteOptionsException extends \BadFunctionCallException
{
    public function __construct($excluded)
    {
        $message = "The option {$excluded} must be set and not empty";
        parent::__construct($message);
    }
}