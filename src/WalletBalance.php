<?php


namespace Emmannl\MobileNg;


class WalletBalance
{
    use HttpRequest, Initialiser;

    protected const BALANCE_API_URL = "https://mobilenig.com/API/balance";


    /**
     * Get wallet balance
     * @return mixed
     * @throws Exception\RemoteServiceException
     */
    public function retrieve()
    {
        $result = $this->get(self::BALANCE_API_URL, [
            'username' => $this->username, 'api_key' => $this->api_key,
        ]);

        $result = json_decode($result);

        return $result->balance;
    }

    public function buy(array $options)
    {
        // TODO: Implement buy() method.
    }

    public function getProductsAndPrices()
    {
        // TODO: Implement getProductsAndPrices() method.
    }
}