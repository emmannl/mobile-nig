<?php


namespace Emmannl\MobileNg;


trait Initialiser
{
    /**
     * @var string
     */
    protected $username;
    /**
     * @var string
     */
    protected $api_key;
    /**
     * @var bool
     */
    protected $test_mode;

    /**
     * @var array
     */
    protected $credentials = [];
    /**
     * @var bool
     */
    protected $throw_exceptions;

    public function __construct(string $username, string $api_key, bool $test_mode = false, bool $throw_exceptions = true)
    {
        $this->test_mode = $test_mode;

        $this->credentials = [
            'username' => $this->username = $username,
            'api_key' => $this->api_key =  $api_key,
        ];

        $this->throw_exceptions = $throw_exceptions;
    }
}